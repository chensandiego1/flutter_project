import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:getx_view/App_Controller.dart';

import 'First_Controller.dart';
import 'Home.dart';
import 'Home_Controller_Binding.dart';
import 'Second_Controller.dart';

void main() {
  AppController().dependencies();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        //initialBinding: SecondController(),
        title: "Binding getx",
        // getPages: [
        //   GetPage(
        //     name: "/home",
        //     page: () => Home(),
        //     binding: HomeControllerBinding(),
        //   ),
        // ],
        getPages: [
          GetPage(
              name: '/home',
              page: () => Home(),
              binding: BindingsBuilder(() => {
                    Get.lazyPut<HomeControllerBinding>(
                        () => HomeControllerBinding())
                  })),
        ],
        home: Scaffold(
          appBar: AppBar(
            title: Text("binding getx"),
          ),
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Obx(
                () => Text(
                  "Value is ${Get.find<FirstController>().count}",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(height: 10.0),
              ElevatedButton(
                onPressed: () {
                  Get.find<FirstController>().increment();
                },
                child: Text("Increment button"),
              ),
              SizedBox(height: 10.0),
              ElevatedButton(
                  onPressed: () {
                    // Get.to(Home());

                    //Get.toNamed("/home");
                    Get.to(Home(), binding: HomeControllerBinding());
                  },
                  child: Text("Home"))
            ],
          )),
        ));
  }
}
