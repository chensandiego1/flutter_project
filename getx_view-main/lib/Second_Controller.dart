import 'package:get/get.dart';

import 'First_Controller.dart';
import 'Home_Getx.dart';

class SecondController implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FirstController>(() => FirstController());
    Get.lazyPut<HomeGetx>(() => HomeGetx());
  }
}
