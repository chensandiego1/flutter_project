import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

import 'Home_Getx.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("home"),
        ),
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Obx(
              () => Text(
                "The value is ${Get.find<HomeGetx>().count}",
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Get.find<HomeGetx>().increment();
              },
              child: Text("Increment button"),
            ),
            SizedBox(height: 10.0),
            ElevatedButton(
                onPressed: () {
                  Get.back();
                },
                child: Text("Back"))
          ],
        )));
  }
}
