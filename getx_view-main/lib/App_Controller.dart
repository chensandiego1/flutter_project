import 'package:get/get.dart';
import 'package:getx_view/First_Controller.dart';

class AppController implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FirstController>(() => FirstController());
  }
}
