import 'package:get/get.dart';
import './Home_Getx.dart';

class HomeControllerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeGetx>(() => HomeGetx());
  }
}
