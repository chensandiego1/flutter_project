import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_fb/configuration.dart';
import 'package:getx_fb/rounded_elevated_button.dart';

import 'auth_controller.dart';
import 'main.dart';
import 'rounded_text_formfield.dart';

class ResetPassword extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  final _authController = Get.find<AuthController>();

  ResetPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: SafeArea(
                child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Configuration.screenWidth! * 0.04),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerRight,
                          child: IconButton(
                            icon: Icon(
                              Icons.close_rounded,
                              color: Colors.white,
                            ),
                            onPressed: () => Get.back(),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          'If you want to recover your account,please provide your email id below',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                        SizedBox(height: Configuration.screenHeight! * 0.05),
                        Image.asset(
                          "assets/forgetpass.png",
                          width: 350.0,
                        ),
                        SizedBox(height: Configuration.screenHeight! * 0.05),
                        RoundedTextFormField(
                          controller: _emailController,
                          hintText: "Email",
                          validator: (value) {
                            bool _isEmailValid =
                                RegExp(regExpression).hasMatch(value!);
                            if (!_isEmailValid) {
                              return 'Invalid email';
                            }
                            return null;
                          },
                        ),
                        SizedBox(height: Configuration.screenHeight! * 0.05),
                        RoundedElevatedButton(
                          title: "Send Link",
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              _authController
                                  .resetPassword(_emailController.text.trim());
                            }
                          },
                          padding: EdgeInsets.symmetric(
                            horizontal: Configuration.screenHeight! * 0.08,
                            vertical: Configuration.screenWidth! * 0.01,
                          ),
                        ),
                        SizedBox(height: Configuration.screenHeight! * 0.1),
                      ],
                    ))),
          ),
        ));
  }
}
