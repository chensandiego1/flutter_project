import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_fb/auth_controller.dart';
import 'package:getx_fb/configuration.dart';
import 'package:getx_fb/signup.dart';
import 'package:getx_fb/text_with_textbutton.dart';
import 'package:flutter_pw_validator/flutter_pw_validator.dart';
import 'main.dart';
import 'reset_password.dart';
import 'rounded_elevated_button.dart';
import 'rounded_text_formfield.dart';

class SignIn extends StatefulWidget {
  SignIn({Key? key}) : super(key: key);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  final _authController = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: SingleChildScrollView(
            child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Image.asset("assets/login_pic.png"),
            ),
            RoundedTextFormField(
              controller: _emailController,
              hintText: "Phone number",
              validator: (value) {
                // bool _isEmailValid = RegExp(regExpression).hasMatch(value!);
                //bool _isEmailValid = RegExp(phonepatttern).hasMatch(value!);
                bool _isEmailValid = RegExp(phonePattern).hasMatch(value!);

                if (!_isEmailValid) {
                  return '手機格式錯誤,請輸入09XXXXXXXX';
                }
                return null;
              },
            ),
            SizedBox(height: 20.0),
            RoundedTextFormField(
              controller: _passwordController,
              obsecureText: true,
              hintText: "Password",
              validator: (value) {
                bool _isStrongPasslValid = RegExp(passPattern).hasMatch(value);

                if (value.toString().length < 8) {
                  return 'Password shoud be longer or equal to 8 charachers';
                } else if (!_isStrongPasslValid) {
                  return 'Need at least 1 Character and 1 numerical ';
                }
                return null;
              },
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: Configuration.screenHeight! * 0.05),
              child: Align(
                alignment: Alignment.centerRight,
                child: TextButton(
                  child: Text(
                    'Forget Password?',
                    style: TextStyle(color: Colors.green),
                  ),
                  onPressed: () => Get.to(() => ResetPassword()),
                  style: ButtonStyle(
                    overlayColor:
                        MaterialStateColor.resolveWith((states) => Colors.grey),
                  ),
                ),
              ),
            ),
            RoundedElevatedButton(
              title: 'Login',
              padding: EdgeInsets.symmetric(
                  horizontal: Configuration.screenWidth! * 0.3,
                  vertical: Configuration.screenHeight! * 0.02),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  String email = _emailController.text.trim();
                  // String password = _passwordController.text;
                  String password =
                      'cc2e018aa6eb9612ccd027bbdcdc9b8c8d351789f14cae4d688a876c18938235';
                  _authController.signInWithSpace(email, password);

                  // _authController.signInUsingFirebaseAuth(email, password);
                }
              },
            ),
            SizedBox(height: Configuration.screenHeight! * 0.01),
            TextWithTextButton(
              text: "Do not have an account?",
              textButtonText: 'Sign up here',
              onPressed: () => Get.to(() => SignUp()),
            ),
            TextWithTextButton(
              text: "",
              textButtonText: 'Or',
              onPressed: () => print(""),
            ),
            SizedBox(height: Configuration.screenHeight! * 0.01),
            GestureDetector(
              onTap: () {
                _authController.signInUserWithGoogle();
              },
              child: Image.asset(
                "assets/signin_google.png",
                width: 200.0,
              ),
            ),
            SizedBox(height: Configuration.screenHeight! * 0.01),
          ],
        )));
  }
}
