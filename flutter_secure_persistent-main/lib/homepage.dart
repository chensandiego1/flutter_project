import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './services/storage.dart';

class Homepage extends StatefulWidget {
  Homepage({Key? key}) : super(key: key);

  @override
  State<Homepage> createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  final SecureStorage secureStorage = SecureStorage();

  late String myjwt;
  late String myusername;
  late String myexpreidtime;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    init();
  }

  Future init() async {
    final jwt = await secureStorage.readSecureData('jwt');

    final username = await secureStorage.readSecureData('name');
    final expiredTime = await secureStorage.readSecureData('expiredTime');

    setState(() {
      myjwt = jwt;
      myusername = username;
      myexpreidtime = expiredTime;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('secure storage')),
      body: Column(
        children: [
          Text(
            myjwt,
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "客戶名",
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            myusername,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            myexpreidtime,
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
