import 'package:flutter/material.dart';
import 'services/storage.dart';
import './homepage.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final SecureStorage secureStorage = SecureStorage();
  String? _timeString;
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    int? _timeString =
        int.parse(DateTime.now().toUtc().millisecondsSinceEpoch.toString()) +
            7776000000;
    secureStorage.writeSecureData('jwt',
        'g7rDbJNrIOrsNTH-b68Wzk3dqTUDHbppc0I6y8chW-Hl76DA1jUNhxHFar7n-BS2aVm3zexhA2t8cktoHC0LI2sZc38R_inxfc3Q');
    secureStorage.writeSecureData('name', 'demo');
    secureStorage.writeSecureData('expiredTime', _timeString.toString());
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Homepage(),
    );
  }
}
