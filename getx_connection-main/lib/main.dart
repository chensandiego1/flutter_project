import 'package:flutter/material.dart';
import 'package:get/get.dart';
import './getxNetworkManager.dart';
import './NetworkBinding.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Network Detect',
      initialBinding: NetworkBinding(),
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GetXNetworkManager _networkManager = Get.find<GetXNetworkManager>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Network Status',
          style: TextStyle(fontSize: 30),
        ),
        GetBuilder<GetXNetworkManager>(
            builder: (builder) => Text(
                  (_networkManager.connectionType == 0)
                      ? 'No Internet'
                      : (_networkManager.connectionType == 1)
                          ? 'You are Connected to Wifi'
                          : 'You are Connected to Mobile Internet',
                  style: TextStyle(fontSize: 30),
                )),
      ],
    )));
  }
}
